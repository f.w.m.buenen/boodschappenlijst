﻿using System;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Boodschappenlijst
{
    public class homeWindow : Form
    {
    	// the following array is an example selection of products which the client buys regularly and which are display in the window
        string[] boodschappen = {"aardappels", "uien", "champignons", "cherrytomaatjes", "groente", "sinaasappels", "kiwi's",
                                "appels", "bananen", "rundergehakt", "vlees", "kaasplakken", "vleesbeleg", "franse kaas", 
                                "toast salade", "olijven", "melk", "activia", "toetje", "boter", "bakboter", "eieren", "brood",
                                "appelstroop", "pindakaas", "hagelslag", "jam", "beschuit", "ontbijtkoek", "toastjes", "cola",
                                "jus d'orange", "appelsap", "thee", "koffie snelfiltermaling", "koffiepads", "koffiefilters",
                                "koffiemelk", "rode wijn", "witte wijn", "bier", "bouillonblokjes", "kruiden", "knabbeltjes", 
                                "koeken", "snoep", "toiletpapier", "afwasmiddel", "keukenrol"};
        string[] uiteindelijkeProducten;
        string[] uiteindelijkeHvh;
        TextBox[] hoeveelheden;
        CheckBox producten;
        CheckBox[] checkboxProducten;
        TextBox overigeProducten;
        TextBox hoeveelheid;

        public homeWindow()
        {
            this.Text = "Boodschappenlijst";
            this.ClientSize = new Size(1050, 670);
            this.BackColor = Color.FloralWhite;

            Button verstuur = new Button();
            verstuur.Text = "Verstuur";
            verstuur.Font = new Font("Verdana", 20);
            verstuur.Location = new Point(40, 600);
            verstuur.Size = new Size(200, 30);
            verstuur.BackColor = Color.LightGray;

            Label overigLabel = new Label();
            overigLabel.Text = "Overige\nproducten";
            overigLabel.Font = new Font("Verdana", 20);
            overigLabel.Location = new Point(40, 500);
            overigLabel.Size = new Size(200, 100);

            overigeProducten = new TextBox();
            overigeProducten.Font = new Font("Verdana", 15);
            overigeProducten.Location = new Point(270, 480);
            overigeProducten.Size = new Size(500, 130);
            overigeProducten.Multiline = true;

            checkboxProducten = new CheckBox[boodschappen.Length];
            hoeveelheden = new TextBox[boodschappen.Length];

            int xcheckbox = 40;
            int ycheckbox = 20;

            // boodschappen overzicht toevoegen
            for(int i = 1; i<=boodschappen.Length; i++)
            {
                producten = new CheckBox();
                producten.Appearance = Appearance.Normal;
                producten.AutoCheck = true;
                producten.Text = boodschappen[i-1];
                producten.Font = new Font("Verdana", 11);
                producten.Size = new Size(150, 40);
                producten.Location = new Point(xcheckbox, ycheckbox);
                checkboxProducten[i - 1] = producten;

                hoeveelheid = new TextBox();
                hoeveelheid.Location = new Point(xcheckbox + 150, ycheckbox);
                hoeveelheid.Font = new Font("Verdana", 10);
                hoeveelheid.Size = new Size(60, 5);
                hoeveelheden[i - 1] = hoeveelheid;

                ycheckbox = ycheckbox + 35;
                this.Controls.Add(producten);
                this.Controls.Add(hoeveelheid);

                if (i % 13 == 0) 
                { 
                    xcheckbox = xcheckbox + 250;
                    ycheckbox = 20;
                };
            }

            uiteindelijkeProducten = new string[boodschappen.Length];
            uiteindelijkeHvh = new string[boodschappen.Length];

            verstuur.Click += checkSelected;

            this.Controls.Add(verstuur);
            this.Controls.Add(overigLabel);
            this.Controls.Add(overigeProducten);
        }

        private void checkSelected(Object sender, EventArgs e)
        {
            int teller = 0;

            for (int j = 0; j<checkboxProducten.Length; j++)
            {
                if (checkboxProducten[j].Checked)
                {
                    uiteindelijkeProducten[teller] = checkboxProducten[j].Text;
                    uiteindelijkeHvh[teller] = hoeveelheden[j].Text;
                    teller++;
                }
            }

            Form berichtTest = new Form();
            berichtTest.ClientSize = new Size(600, 200);

            Label berichtOutput = new Label();
            berichtOutput.Size = new Size(500, 180);
            berichtOutput.Location = new Point(20, 20);

            string tekst = "Boodschappenlijst: <BR>";

            for (int k = 0; k < uiteindelijkeProducten.Length; k++ )
            {
                if (uiteindelijkeProducten[k] == null) { break; };
                tekst = tekst + "- " + uiteindelijkeProducten[k] + " " + uiteindelijkeHvh[k] + "<BR>";
            }

            tekst = tekst + "<BR>" + "Extra's: <BR>" + overigeProducten.Text;

            this.verstuurMail(tekst);
        }

        private void verstuurMail(string tekst)
        {
            SmtpClient smtpClient = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential("CLIENT-EMAIL", "CLIENT-PASSWORD");
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress("CLIENT-EMAIL");

            smtpClient.Host = "CLIENT HOST";
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = basicCredential;
            smtpClient.EnableSsl = true;

            message.From = fromAddress;
            message.Subject = "Boodschappenlijst";
            message.IsBodyHtml = true;
            message.Body = tekst;
            message.To.Add("CLIENT HELP E-MAIL ADDRESS");

            try
            {
                smtpClient.Send(message);
                if (MessageBox.Show("Het boodschappenlijstje is verstuurd!", "Verstuurd", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk) == DialogResult.OK)
                {
                    uiteindelijkeProducten = new string[boodschappen.Length];
                };
            }
            catch (Exception ex)
            {
                //Error, could not send the message
                MessageBox.Show(ex.Message);
            }

        }
    }
}
